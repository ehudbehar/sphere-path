# Paths on a sphere

GeoGebra graphing of 3 points on a circle and the parametric curve joining each pair.

GeoGebra commands:
`S=Sphere((0,0,0),1)`
`A=PointIn(S)`
`B=PointIn(S)`
`C=PointIn(S)`


## x-y plane path
Now I can draw the curve between two points only if they lay on the $`x-y`$ planes:
```math
\begin{pmatrix}x(t)\\y(t) 
\end{pmatrix}
=
\begin{pmatrix}
\cos(t) \\ \sin(t)
\end{pmatrix}
\text{where } t\in\left[ \tan^{-1}\frac{y_A}{x_A},  \tan^{-1}\frac{y_B}{x_B}\right]
```
`a=Curve(cos(t),sin(t),0,t,t1,t2)` with `t1=atan2(y(A),x(A))` and `t2=atan2(y(B),x(B))`

It is OK up to point where $`t_f>t_i`$. Try to fix it with a `min()` and `max()` function:

`Curve(cos(t), sin(t), 0, t, Min(t1, t2), Max(t1, t2))` better not that good too. I think it is rather a `atan` or `atan2` issue that is needed to be solved.

Also, I want to draw the acute angle joining the two points.

## Plane angle of tilt
To help you see the curve you are looking for, create a plane passing between them:
`p=Plane((0,0,0),A,B)`.
The unit normal vector to this plane (passing through the origin, A and B is)
`u=UnitVector(Direction(Plane(A,(0,0,0),B)))`

But what is the angle to be rotated around so that the curve passes through the two points?

## Another idea:
Plot a ring on the sphere in x-y plane:
`b_{1}=Curve(cos(t),sin(t),0,t,0,2π)`
Now rotate it with the 3 matrices for yaw, pitch and roll described  [here](https://en.m.wikipedia.org/wiki/Rotation_matrix#General_rotations).
 - Define angles α, β and γ running from 0 to 360
 - Matrix `yaw = {{cos(α),-sin(α),0},{sin(α),cos(α),0},{0,0,1}}`
 - Matrix `pitch = {{cos(β),0,sin(β)},{0,1,0},{-sin(β),0,cos(β)}}`
 - Matrix `roll = {{1,0,0},{0,cos(γ),-sin(γ)},{0,sin(γ),cos(γ)}}`
 - The matrix product of the 3: `rot= {{cos(α)cos(β),cos(α)sin(β)sin(γ)-sin(α)cos(γ),cos(α)sin(β)cos(γ)+sin(α)sin(γ)},{sin(α)cos(β),sin(α)sin(β)sin(γ)+cos(α)cos(γ),sin(α)sin(β)cos(γ)-cos(α)sin(γ)},{-sin(β),cos(β)sin(γ),cos(β),cos(γ)}}`

Rotate the ring $`c`$ a rotation whose yaw, pitch, and roll angles are α, β and γ, respectively: `c_{4}=Curve(Element(rot,1,1)x(c(t))+Element(rot,1,2)y(c(t))+Element(rot,1,3)z(c(t)),Element(rot,2,1)x(c(t))
+Element(rot,2,2)y(c(t))+Element(rot,2,3)z(c(t)),Element(rot,3,1)x(c(t))+Element(rot,3,2)y(c(t))+Element(rot,3,3)z(c(t)),t,0,10)`

(To plot the curve rotated around each one of the above directions, replace `rot` by the name of the rotation).

Now we need to find the relation between α, β and γ to the vector $`u`$.

If you define $`γ`$ to be $`\cos^{-1} u_z`$ then it restricts one degree of freedom.

It looks like you can set β to be zero, i.e. no pitch rotation, and then play a bit with the roll and coincide the ring with the points A and B. The question is that this γ equals to in terms of the vecotr v components.

Try first to apply the yaw rotation, and only then to apply the pitch rotation. As it is now the rotation is "parabolic" or something like that.

I tried pitch and then yawn and I succeeded orienting it.

### Trial 1
Roll->Pitch, with use of gamma and then beta only:
γ=352° and β=314°, for u=(0.71,-.14,-.69)
### Trial 2
Pitch->Roll, with use of gamma and then beta only:
β=30° and γ=214° and , for u=(0.5,0.48,-.71)

### Solved!
Use the yaw and pitch matrices:
`yawpitch = {{cos(α) cos(β), -sin(α), cos(α) sin(β)}, {cos(β) sin(α), cos(α), sin(α) sin(β)}, {-sin(β), 0, cos(β)}}`

i.e. multiply yaw.pitch: $`Yaw.Pitch`$.

`cyawpitch = Curve((yawpitch c(t)), t, 0, 2π)`

 - First "pitch" by β an amount of $`\cos^{-1}\theta`$:
`acosd(z(u))`;
 - and then "yaw" by α an amount of $`\tan^{-1}\frac{y}{x}`$: `atan2d(y(u), x(u))`.

 ## Now try to find the curve that joins the two points as described in x-y plane path section.

 ## List of matrices combinations:
 
1. `rollpitch = {{cos(β), 0, sin(β)}, {sin(γ) sin(β),   cos(γ), -cos(β) sin(γ)}, {-cos(γ) sin(β), sin(γ), cos(γ) cos(β)}}`
2. `pitchroll = {{cos(β), sin(γ) sin(β), cos(γ) sin(β)}, {0,   cos(γ), -sin(γ)}, {-sin(β), cos(β) sin(γ), cos(γ) cos(β)}}`
3. `rollyaw =   {{cos(α), -sin(α), 0}, {cos(γ) sin(α),   cos(γ) cos(α), -sin(γ)}, {sin(γ) sin(α), cos(α) sin(γ), cos(γ)}}`
4. `yawroll =   {{cos(α), -cos(γ) sin(α), sin(γ) sin(α)}, {sin(α),   cos(γ) cos(α), -cos(α) sin(γ)}, {0,   sin(γ), cos(γ)}}`
5. `yawpitch =  {{cos(α) cos(β), -sin(α), cos(α) sin(β)}, {cos(β) sin(α), cos(α), sin(α) sin(β)}, {-sin(β), 0, cos(β)}}`
6. `pitchyaw =  {{cos(α) cos(β), -cos(β) sin(α), sin(β)}, {sin(α), cos(α), 0}, {-cos(α) sin(β), sin(α) sin(β), cos(β)}}`

## Looks like rollpitch is the most intuitive rotation. First γ then β. Now I need to find out what are those angles in terms of $`u`$ components.
 - Let `U=Point({x(u), y(u), z(u)})`
 - Let `ε=Angle(U,Point({0,0,0}),Point({1,0,0}))`
 - If `β=ε-90°` and both points are located with positive z values i.e. above the x-y plane,
 then you can find a `γ` that passes through both points.

 But I think it is better to try to find the angle with 