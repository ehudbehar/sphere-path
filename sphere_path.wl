(* ::Package:: *)

Manipulate[
Show[{Graphics3D[{
Point[{Cos[\[Phi]]Sin[\[Theta]],Sin[\[Theta]]Sin[\[Phi]],Cos[\[Theta]]}]
,PlotRange->{{-1,1},{-1,1},{-1,1}},
Sphere[]}]
,
ParametricPlot3D[{Cos[\[Theta]],Sin[\[Theta]],0},{\[Theta],0,2\[Pi]}]}]
,{\[Theta],0,\[Pi]},{\[Phi],0,2\[Pi]}
]


Show[{Graphics3D[{
Point[Table[{Cos[#2]Sin[#1],Sin[#2] Sin[#1],Cos[#1]}&[(*\[Phi]*)RandomReal[{0,2\[Pi]}],(*\[Theta]*)RandomReal[{0,\[Pi]}]],2]],PlotRange->{{-1,1},{-1,1},{-1,1}},Sphere[]
}],
ParametricPlot3D[{Cos[\[Theta]],Sin[\[Theta]],0},{\[Theta],0,2\[Pi]}]},
Axes->True,
Boxed->False,
AxesStyle->Black,
AxesOrigin->{0,0,0},
AxesLabel->{x,y,z},
Ticks->None]


asso=<|
\[Theta]1->RandomReal[{0,\[Pi]}],
\[Phi]1->RandomReal[{0,2\[Pi]}],
\[Theta]2->RandomReal[{0,\[Pi]}],
\[Phi]2->RandomReal[{0,2\[Pi]}]
|>;
points=ReplaceAll[asso][{
{Cos[\[Phi]1]Sin[\[Theta]1],Sin[\[Phi]1]Sin[\[Theta]1],Cos[\[Theta]1]},
{Cos[\[Phi]2]Sin[\[Theta]2],Sin[\[Phi]2]Sin[\[Theta]2],Cos[\[Theta]2]}
}];
plane=InfinitePlane[Append[points,{0,0,0}]]


Graphics3D[
{(*plane,*){PointSize[Large],Point[points]},Sphere[]}
,PlotRange->{{-1,1},{-1,1},{-1,1}},
Axes->True,Boxed->False,AxesStyle->Black,AxesOrigin->{0,0,0},AxesLabel->{x,y,z},Ticks->None]
